Docker OpenERP
==

1. 基于 `ubuntu:12.04`，会尽量频繁地跟进官方更新

2. 公开托管于 [Bitbucket](https://bitbucket.org/hbrls/docker-openerp) 及 [Docker Hub](https://registry.hub.docker.com/u/hbrls/docker-openerp/)

3. 目标是成为一个尽量通用的、可以直接运行 `OpenERP 7.0` 的镜像，会有版本号

4. 出于项目需要，会安装一些第三方类库，但都是公开库，只会造成镜像体积增大，不会影响运行

5. 实际项目会基于此再 build 一个 `hbrls/docker-openerp-activemq:latest` 镜像，不会公开

Current Version: 0.1.0
==
